package com.scania.artist;
import java.util.List;
import java.io.*;
public class Artist {

    private String filePath;

    public Artist(String filePath){

        this.filePath = filePath;
    }

public void printArtist() {

        try (FileReader fileReader = new FileReader(filePath);

             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            System.out.println("\n" + "---------------------------------------------------------------------------------------");

            System.out.println("\n" + "Artists previously entered:" + "\n");

            String line = "";

            while ((line = bufferedReader.readLine()) !=null) {

                System.out.println(line);

            }

        } catch (Exception e){
            System.out.println("\n" + "---------------------------------------------------------------------------------------" + "\n");
            System.out.println("Artist database is currently empty.");
            System.out.println("Let me create one for you!");
        }
}

    public void writeArtist(List<String> artists) {

        try (FileWriter fileWriter = new FileWriter(filePath, true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            for (String artist : artists)

            bufferedWriter.write(artist + "\n");

            }

   catch (Exception e){

        System.out.println(e.getMessage());
    }
    }
}


