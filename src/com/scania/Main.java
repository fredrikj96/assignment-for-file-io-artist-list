package com.scania;

import com.scania.artist.Artist;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Artist artist = new Artist("artists.txt");

        artist.printArtist();

        List<String> artists = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);


        System.out.println("\n" + "----------------------------------------Welcome----------------------------------------" + "\n");

        System.out.println("1. To add an artist/band to your database, simply type the name of the artist/band.");
        System.out.println("Artists:" + "\n");

        System.out.println("2. If you rather go do something else, simply type \"exit\" and this program will quit."+ "\n");
        System.out.print("Input: ");

        String inputChoice = scanner.nextLine();



       while (!inputChoice.equals("exit") && !inputChoice.equals("EXIT")) {

           System.out.print("Nice one, hit me with some more! :");
           artists.add(inputChoice);
           inputChoice = scanner.nextLine();

       }

        System.out.println("\n" + "See ya!");
        System.out.println("\n" + "---------------------------------------------------------------------------------------" + "\n");
        artist.writeArtist(artists);

    }
}